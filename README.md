# Fitpeo Test

Project is deployed on **https://fitpeowebdevtest.netlify.app/**.

Assignment Link - **https://docs.google.com/document/d/1GgJjabTpPh1tz-8RPH1OIW5ikDo3B_Fc/edit**.

# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

## Authors and acknowledgment
Pranay shah
