import React from 'react'
import {BsSearch} from 'react-icons/bs';
import {MdOutlineKeyboardArrowDown} from 'react-icons/md';
import random1 from "../assets/random-1.JPG";
import random2 from "../assets/random-2.JPG";

const Products = () => {
  return (
    <div className='products'>
        <div className='products-top'>
            <div><h2>Product Sell</h2></div>
            <div className='products-right'>
              <div className='product-search'>
                <BsSearch className='icon' />
                <input type='search' placeholder='Search' aria-label='Search for anything' />
              </div>
                <span className='dropdown'>Last 30 days <MdOutlineKeyboardArrowDown /> </span>
            </div>
        </div>
        <div className='products-detail'>    
            <span>Product Name</span>
            <span>Stock</span>
            <span>Price</span>
            <span>Total Sales</span>
        </div>

        <div className='all-products'>
            <div className='product'>
              <div><img src={random1} style={{height: '40px', width : '80px', borderRadius: '8px'}}/></div>
              <div>
                  <p className='bold'>Abstract 3D</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              </div>
            </div>
            <span>32 in stock</span>
            <span className='bold'>$45.99</span>
            <span>20</span>
        </div>
          
        <div className='all-products'>
            <div className='product'>
              <div><img src={random2} style={{height: '40px', width : '80px', borderRadius: '8px'}}/></div>
              <div>
                  <p className='bold'>Sarphens Illustration</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              </div>
            </div>
            <span>32 in stock</span>
            <span className='bold'>$45.99</span>
            <span>20</span>
        </div>
    </div>
  )
}

export default Products