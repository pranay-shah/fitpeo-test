import React from 'react';
import {BsSearch} from 'react-icons/bs';
import {GiHamburgerMenu} from 'react-icons/gi';


const Header = ({openSideBar}) => {
  return (
    <header className='header'>
      <div className='menu-icon'>
        <GiHamburgerMenu className='icon' onClick={openSideBar}/>
      </div>
        <h1 className='sidebar-brand'>Hello Shahrukh👋🏻,</h1>
        <div className='header-right'>
          <BsSearch className='icon' />
          <input type='search' placeholder='Search' aria-label='Search for anything' />
        </div>
    </header>
  )
}

export default Header