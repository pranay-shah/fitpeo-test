import React from 'react'
import {TbSettings2, TbUserSquareRounded} from 'react-icons/tb';
import {HiOutlineKey} from 'react-icons/hi';
import {PiCubeFocusBold} from 'react-icons/pi';
import {IoWallet} from 'react-icons/io5';
import {LiaPercentageSolid} from 'react-icons/lia';
import {MdOutlineLiveHelp, MdOutlineKeyboardArrowRight, MdOutlineKeyboardArrowDown} from 'react-icons/md';
import Profile from "../assets/profile.png";

const SideBar = ({sideBarToggle, openSideBar}) => {
  return (
    <aside id="sidebar" className={sideBarToggle ? "sidebar-responsive" : ""}>
        <div className='sidebar-menu'>
            <div className='sidebar-title'>
                <div className='sidebar-brand selected'>
                    <TbSettings2  className='icon_header'/> Dashboard
                </div>
                <span className='icon close_icon' onClick={openSideBar}>X</span>
            </div>
            
            <ul className='sidebar-list'>
                <li className='sidebar-list-item sidebar-active'>
                    <span className='selected'>
                        <HiOutlineKey className='icon'/> Dashboard
                    </span>
                </li>
                <li className='sidebar-list-item'>
                    <span>
                        <PiCubeFocusBold className='icon'/> Products
                    </span>
                    <MdOutlineKeyboardArrowRight />
                </li>
                <li className='sidebar-list-item'>
                    <span>
                        <TbUserSquareRounded className='icon'/> Customers
                    </span>
                    <MdOutlineKeyboardArrowRight />
                </li>
                <li className='sidebar-list-item'>
                    <span>
                        <IoWallet className='icon'/> Income
                    </span>
                    <MdOutlineKeyboardArrowRight />
                </li>
                <li className='sidebar-list-item'>
                    <span>
                        <LiaPercentageSolid className='icon'/> Promote
                    </span>
                    <MdOutlineKeyboardArrowRight />
                </li>
                <li className='sidebar-list-item'>
                    <span>
                        <MdOutlineLiveHelp className='icon'/> Help
                    </span>
                    <MdOutlineKeyboardArrowRight />
                </li>
            </ul>
        </div>

        <div className='sidebar-profile sidebar-active'>
            <div className='profile'>
                <img src={Profile} alt='Profile Picture' className='sidebar-image'/>
                <div>
                <a href='https://pranayshah.netlify.app/'>
                    <h4 className='profile-name selected'>Pranay Shah</h4>
                </a>
                    <span className='profile-title'>Frontend Dev</span>
                </div>
            </div>
            <MdOutlineKeyboardArrowDown />
        </div>
    </aside>

  )
}

export default SideBar