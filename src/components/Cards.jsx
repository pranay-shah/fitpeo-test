import React from 'react'
import {FiArrowUp, FiArrowDown} from "react-icons/fi";
import {TbShoppingBag} from "react-icons/tb";
import {PiCurrencyCircleDollarBold} from "react-icons/pi";
import {TiDocumentText} from "react-icons/ti";
import {IoWalletOutline} from "react-icons/io5";

const Cards = () => {
  return (
    <div className='main-cards'>
        <div className='card'>
          <PiCurrencyCircleDollarBold size={50} className='card-icon' color='#22A717'/>
          <div className='card-details'>
            <p className='card-text'>Earning</p>
            <p className='card-price'>$198k</p>
            <p className='card-text'>
              <span className='card-text green'><FiArrowUp />37.8% </span>
              this month
            </p>
          </div>
        </div>
        <div className='card'>
          <TiDocumentText size={50} className='card-icon' color='#9236A8'/>
          <div className='card-details'>
            <p className='card-text'>Orders</p>
            <p className='card-price'>$2.4k</p>
            <p className='card-text'>
              <span className='card-text red'><FiArrowDown />2% </span>
              this month
            </p>
          </div>
        </div>
        <div className='card'>
          <IoWalletOutline size={50} className='card-icon' color='#0458AB'/>
          <div className='card-details'>
            <p className='card-text'>Balance</p>
            <p className='card-price'>$2.4k</p>
            <p className='card-text'><span className='card-text red'><FiArrowDown />2% </span>this month</p>
          </div>
        </div>
        <div className='card'>
          <TbShoppingBag size={50} className='card-icon' color='#FF0000'/>
          <div className='card-details'>
            <p className='card-text'>Total Sales</p>
            <p className='card-price'>$89k</p>
            <p className='card-text'><span className='card-text green'><FiArrowUp />11% </span>this month</p>
          </div>
        </div>
    </div>
  )
}

export default Cards