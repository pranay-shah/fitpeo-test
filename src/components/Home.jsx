import React from 'react'
import Bargraph from "../assets/bg.JPG";
import PieChart from "../assets/pc.JPG";
import {MdOutlineKeyboardArrowDown} from 'react-icons/md';
import Cards from './Cards';
import Products from './Products';


const Home = () => {

  return (
    <main className='main-container'>
      <Cards />

      <div className='charts'>
        <div className='charts-description'>
          <div className='charts-position'>
            <div>
              <h2>Overview</h2>
              <p>Monthly Earning</p>
            </div>
            <div className='dropdown flex'>
                Quarterly <MdOutlineKeyboardArrowDown />
            </div>
          </div>
          <div className='bargraph-img'>
            <img src={Bargraph} style={{width: '100%', height:'100%'}}/>
          </div>
        </div>

        <div className='charts-description'>
          <div className='charts-position'>
            <div>
              <h2>Customers</h2>
              <p>Customers that buy products</p>
            </div>
          </div>
          <div className='piechart-img'>
            <img src={PieChart} style={{width: '100%', height:'100%'}}/>
          </div>
        </div>
      </div>
       
      <Products />
    </main>
  )
}

export default Home