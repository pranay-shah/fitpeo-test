import { useState } from 'react'
import './App.css'
import Header from './components/Header'
import Home from './components/Home'
import SideBar from './components/SideBar'

function App() {
  const [sideBarToggle, setSideBarToggle]  = useState(false);

  const openSideBar = () =>{
    setSideBarToggle(!sideBarToggle);
  }
  return (
    <div className='grid-container'>
      <Header openSideBar={openSideBar}/>
      <SideBar sideBarToggle={sideBarToggle} openSideBar={openSideBar}/>
      <Home />
    </div>
  )
}

export default App
